<!-- Copyright (c)  2019-2021 The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Changes from Qualcomm Innovation Center are provided under the following license:
Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
SPDX-License-Identifier: BSD-3-Clause-Clear

-->
<manifest version="1.0" type="device" target-level="6">
    <kernel target-level="6"/>
    <!-- CapabilityConfigStore HAL Service  -->
    <hal format="hidl">
        <name>vendor.qti.hardware.capabilityconfigstore</name>
        <transport>hwbinder</transport>
        <version>1.0</version>
        <interface>
            <name>ICapabilityConfigStore</name>
            <instance>default</instance>
        </interface>
    </hal>
    <hal format="hidl">
        <name>android.hardware.audio.effect</name>
        <transport>hwbinder</transport>
        <version>7.0</version>
        <interface>
            <name>IEffectsFactory</name>
            <instance>default</instance>
        </interface>
    </hal>
    <hal format="hidl">
        <name>android.hardware.media.omx</name>
        <transport>hwbinder</transport>
        <impl level="generic"></impl>
        <version>1.0</version>
        <interface>
            <name>IOmx</name>
            <instance>default</instance>
        </interface>
        <interface>
            <name>IOmxStore</name>
            <instance>default</instance>
        </interface>
    </hal>
    <!-- DSP Service -->
    <hal format="hidl">
        <name>vendor.qti.hardware.dsp</name>
        <transport>hwbinder</transport>
        <version>1.0</version>
        <interface>
            <name>IDspService</name>
            <instance>dspservice</instance>
        </interface>
    </hal>
    <!-- fingerprint hal: using remote service instead of Google's default service-->
    <hal format="hidl">
        <name>vendor.qti.hardware.fingerprint</name>
        <transport>hwbinder</transport>
        <version>1.0</version>
        <interface>
            <name>IQtiExtendedFingerprint</name>
            <instance>default</instance>
        </interface>
    </hal>
    <hal format="hidl">
        <name>android.hardware.keymaster</name>
        <transport>hwbinder</transport>
        <fqname>@4.1::IKeymasterDevice/default</fqname>
        <fqname>@4.0::IKeymasterDevice/strongbox</fqname>
    </hal>
    <hal format="hidl">
       <name>android.hardware.gatekeeper</name>
       <transport>hwbinder</transport>
       <impl level="generic"></impl>
       <version>1.0</version>
       <interface>
           <name>IGatekeeper</name>
           <instance>default</instance>
       </interface>
    </hal>
    <!-- QSEECom HAL service -->
    <hal format="hidl">
        <name>vendor.qti.hardware.qseecom</name>
        <transport>hwbinder</transport>
        <version>1.0</version>
        <interface>
            <name>IQSEECom</name>
            <instance>default</instance>
        </interface>
    </hal>
    <!-- QTEEConnector HAL service -->
    <hal format="hidl">
        <name>vendor.qti.hardware.qteeconnector</name>
        <transport>hwbinder</transport>
        <version>1.0</version>
        <interface>
            <name>IAppConnector</name>
            <instance>default</instance>
        </interface>
        <interface>
            <name>IGPAppConnector</name>
            <instance>default</instance>
        </interface>
    </hal>
    <!-- SensorCal HAL service -->
    <hal format="hidl">
        <name>vendor.qti.hardware.sensorscalibrate</name>
        <transport>hwbinder</transport>
        <version>1.0</version>
        <interface>
            <name>ISensorsCalibrate</name>
            <instance>default</instance>
        </interface>
    </hal>

    <!-- Factory HAL service -->
    <hal format="hidl">
        <name>vendor.qti.hardware.factory</name>
        <transport>hwbinder</transport>
        <version>1.1</version>
        <interface>
            <name>IFactory</name>
            <instance>default</instance>
        </interface>
    </hal>
    <!-- IOP HAL service -->
    <hal format="hidl">
        <name>vendor.qti.hardware.iop</name>
        <transport>hwbinder</transport>
        <version>2.0</version>
        <interface>
            <name>IIop</name>
            <instance>default</instance>
        </interface>
    </hal>
    <!-- bluetooth -->
    <hal format="hidl">
        <name>android.hardware.bluetooth.audio</name>
        <transport>hwbinder</transport>
        <version>2.0</version>
        <interface>
            <name>IBluetoothAudioProvidersFactory</name>
            <instance>default</instance>
        </interface>
    </hal>
    <hal format="hidl">
        <name>vendor.qti.hardware.bluetooth_audio</name>
        <transport>hwbinder</transport>
        <version>2.1</version>
        <interface>
            <name>IBluetoothAudioProvidersFactory</name>
            <instance>default</instance>
        </interface>
    </hal>
    <hal format="hidl">
        <name>vendor.qti.hardware.btconfigstore</name>
        <transport>hwbinder</transport>
        <version>2.0</version>
        <interface>
            <name>IBTConfigStore</name>
            <instance>default</instance>
        </interface>
    </hal>
    <!--ANT-->
    <hal format="hidl">
        <name>com.dsi.ant</name>
        <transport>hwbinder</transport>
        <version>1.0</version>
        <interface>
            <name>IAnt</name>
            <instance>default</instance>
        </interface>
    </hal>
    <!--ANT-->
    <!-- Alarm HAL service -->
    <hal format="hidl">
        <name>vendor.qti.hardware.alarm</name>
        <transport>hwbinder</transport>
        <version>1.0</version>
        <interface>
            <name>IAlarm</name>
            <instance>default</instance>
        </interface>
    </hal>

    <!-- Camera PostProcessing service -->
    <hal format="hidl">
        <name>vendor.qti.hardware.camera.postproc</name>
        <transport>hwbinder</transport>
        <version>1.0</version>
        <interface>
            <name>IPostProcService</name>
            <instance>camerapostprocservice</instance>
        </interface>
    </hal>

    <hal format="hidl">
        <name>android.hardware.camera.provider</name>
        <transport>hwbinder</transport>
        <version>2.7</version>
        <interface>
            <name>ICameraProvider</name>
            <instance>legacy/1</instance>
        </interface>
    </hal>

    <!-- WifiStats HAL service -->
    <hal format="hidl">
        <name>vendor.qti.hardware.wifi.wifilearner</name>
        <transport>hwbinder</transport>
        <version>1.0</version>
        <interface>
            <name>IWifiStats</name>
            <instance>wifiStats</instance>
        </interface>
    </hal>
    <!-- QSPM-HAL service-->
    <hal format="hidl">
        <name>vendor.qti.qspmhal</name>
        <transport>hwbinder</transport>
        <version>1.0</version>
        <interface>
            <name>IQspmhal</name>
            <instance>default</instance>
        </interface>
    </hal>
    <!-- BluetoothSar service-->
    <hal format="hidl">
        <name>vendor.qti.hardware.bluetooth_sar</name>
        <transport>hwbinder</transport>
        <version>1.1</version>
        <interface>
            <name>IBluetoothSar</name>
            <instance>default</instance>
        </interface>
    </hal>
    <!-- SPU service-->
    <hal format="hidl">
        <name>vendor.qti.spu</name>
        <transport>hwbinder</transport>
        <version>1.1</version>
        <interface>
            <name>ISPUManager</name>
            <instance>default</instance>
        </interface>
    </hal>
    <hal format="hidl">
        <name>vendor.qti.spu</name>
        <transport>hwbinder</transport>
        <version>2.0</version>
        <interface>
            <name>ISPUManager</name>
            <instance>default</instance>
        </interface>
    </hal>
</manifest>
